﻿

using LogicaGeneraXml.Serialitation;
using LogicaGeneraXml.Utilitarios;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoViamaticaPica.LogicaFacturaElec
{
    public class ProcFacturaXml
    {
        private readonly ClaveAcceso _claveAcceso = new ClaveAcceso();

        public dynamic FacturaXml(dynamic cabFactura, string Version, string codDoc, string ruc, string razonSocial, string nombreComercial, string dirMatriz, string dirEstablecimiento, string contribuyenteEspecial, string obligadoContabilidad)
        {
            try
            {

                dynamic xmlGereration = null;
                if (!cabFactura.Equals(null))
                {
                    factura Factura = new factura();
                    Factura.version = Version;
                    Factura.id = facturaID.comprobante;
                    Factura.idSpecified = true;


                    #region InfoTributaria
                    infoTributaria_FA infoTributariaFA = new infoTributaria_FA();
                    infoTributariaFA.ambiente = cabFactura.ambiente;
                    infoTributariaFA.dirMatriz = dirMatriz; //proviene de Compania                                               
                    infoTributariaFA.nombreComercial = nombreComercial; //proviene de Compania 
                    infoTributariaFA.razonSocial = razonSocial;  //proviene de Compania
                    infoTributariaFA.ruc = ruc;   //proviene de Compania

                    facturaInfoFactura FacturaInfoFactura = new facturaInfoFactura();
                    FacturaInfoFactura.obligadoContabilidad = obligadoContabilidad; //proviene de Compania

                    #endregion

                    if (cabFactura.claveAcceso.Trim().Equals("") || cabFactura.claveAcceso.Trim().Equals(null))
                    {
                        infoTributariaFA.claveAcceso = _claveAcceso.GenerarClaveAccesoDocumento("01",
                                                                                           cabFactura.secuencial.Trim(),
                                                                                           cabFactura.puntoEmision.Trim(),
                                                                                           cabFactura.establecimiento.Trim(),
                                                                                           cabFactura.fechaEmision,
                                                                                           //DateTime.Now.ToString("dd-MM-yyyy").Replace("-", "/"),
                                                                                           ruc.ToString().Trim(),
                                                                                           cabFactura.ambiente);
                    }
                    else
                    {
                        infoTributariaFA.claveAcceso = cabFactura.claveAcceso.Trim();  //proviene de CompRetencion
                    }
                    #region CabFactura

                    infoTributariaFA.estab = cabFactura.establecimiento.Trim();     //proviene de CompRetencion
                    infoTributariaFA.ptoEmi = cabFactura.puntoEmision.Trim();    //proviene de CompRetencion          
                    infoTributariaFA.secuencial = cabFactura.secuencial.Trim();    //proviene de CompRetencion
                    infoTributariaFA.tipoEmision = Convert.ToString(cabFactura.tipoEmision).ToString().Trim();   //proviene de CompRetencion
                    infoTributariaFA.codDoc = codDoc;
                    Factura.infoTributaria = infoTributariaFA;
                    FacturaInfoFactura.fechaEmision = cabFactura.fechaEmision;
                    FacturaInfoFactura.dirEstablecimiento = dirEstablecimiento;
                    FacturaInfoFactura.tipoIdentificacionComprador = cabFactura.tipoIdentificacionComprador.ToString().Trim();

                    if (cabFactura.guiaRemision.ToString().Trim().Length != 0)
                    {
                        FacturaInfoFactura.guiaRemision = cabFactura.guiaRemision.ToString().Trim();
                    }
                    else
                    {
                        FacturaInfoFactura.guiaRemision = null;
                    }

                    FacturaInfoFactura.razonSocialComprador = cabFactura.razonSocialComprador.ToString().Trim().Contains("&amp;") ? cabFactura.razonSocialComprador.ToString().Trim().Replace("&amp;", "") : cabFactura.razonSocialComprador.ToString().Trim().Contains("&") ? cabFactura.razonSocialComprador.ToString().Trim().Replace("&", "Y") : cabFactura.razonSocialComprador.ToString().Trim();
                    FacturaInfoFactura.identificacionComprador = cabFactura.identificacionComprador.ToString().Trim();
                    FacturaInfoFactura.direccionComprador = cabFactura.direccion.ToString().Trim();
                    FacturaInfoFactura.totalSinImpuestos = Convert.ToDecimal(cabFactura.totalSinImpuestos.ToString().Trim());
                    FacturaInfoFactura.totalDescuento = Convert.ToDecimal(cabFactura.totalDescuento.ToString().Trim());
                    FacturaInfoFactura.propina = Convert.ToDecimal(cabFactura.propina.ToString().Trim());
                    FacturaInfoFactura.importeTotal = Convert.ToDecimal(cabFactura.importeTotal.ToString().Trim());

                    if (cabFactura.moneda.ToString().Trim().Length != 0)
                    {
                        FacturaInfoFactura.moneda = cabFactura.moneda.ToString().Trim();
                    }
                    Factura.infoFactura = FacturaInfoFactura;
                    #endregion

                    #region FACTURA_TOTAL_IMPUESTO
                    int cont = 0;
                    int contf = 0;

                    if (cabFactura.totalImpuesto.Count > 0)
                    {
                        facturaInfoFacturaTotalImpuesto[] factTotalImpuestos = new facturaInfoFacturaTotalImpuesto[cabFactura.totalImpuesto.Count];

                        foreach (var totalImpuesto in cabFactura.totalImpuesto)
                        {
                            facturaInfoFacturaTotalImpuesto factTotalImpuesto = new facturaInfoFacturaTotalImpuesto();

                            factTotalImpuesto.codigo = totalImpuesto.codigo.ToString().Trim();
                            factTotalImpuesto.codigoPorcentaje = totalImpuesto.codigoPorcentaje.ToString().Trim();
                            factTotalImpuesto.baseImponible = Convert.ToDecimal(totalImpuesto.baseImponible.ToString().Trim());
                            if (cabFactura.totalImpuesto[0].tarifa.ToString().Trim() != "")
                            {
                                factTotalImpuesto.tarifaSpecified = true;
                                factTotalImpuesto.tarifa = Convert.ToDecimal(totalImpuesto.tarifa.ToString().Trim());
                            }
                            else
                            {
                                factTotalImpuesto.tarifaSpecified = false;
                            }

                            factTotalImpuesto.valor = Convert.ToDecimal(totalImpuesto.valor.ToString().Trim());

                            factTotalImpuestos[cont] = factTotalImpuesto;
                            cont++;
                        }
                        Factura.infoFactura.totalConImpuestos = factTotalImpuestos;
                    }
                    cont = 0;
                    #endregion FACTURA_TOTAL_IMPUESTO

                    #region FACTURA_FORMA_PAGO
                    cont = 0;
                    if (cabFactura.formaPago.Count > 0)
                    {

                        facturaInfoFacturaFormaPago[] factFormaPagos = new facturaInfoFacturaFormaPago[cabFactura.formaPago.Count];



                        foreach (var formaPago in cabFactura.formaPago)
                        {
                            facturaInfoFacturaFormaPago factFormaPago = new facturaInfoFacturaFormaPago();

                            factFormaPago.formaPago = formaPago.formaPago.ToString().Trim();
                            factFormaPago.plazo = int.Parse(formaPago.plazo.ToString().Trim());
                            factFormaPago.total = Convert.ToDecimal(formaPago.total.ToString().Trim());
                            factFormaPago.unidadTiempo = formaPago.unidadTiempo.ToString().Trim();

                            factFormaPagos[cont] = factFormaPago;
                            cont++;
                        }
                        Factura.infoFactura.pagos = factFormaPagos;
                    }
                    cont = 0;
                    #endregion FACTURA_FORMA_PAGO

                    #region FACTURA_DETALLE
                    cont = 0;
                    if (cabFactura.detalleFactura.Count > 0)
                    {

                        // facturaDetalle[] factDetalles = new facturaDetalle[cabFactura.detalleFactura.Count];
                        List<facturaDetalle> factDetalles = new List<facturaDetalle>();


                        facturaDetalle factDetalle = new facturaDetalle();
                        int contFactDetImpuesto = 0;
                        foreach (var detalleFactrura in cabFactura.detalleFactura)
                        {

                            if (detalleFactrura.codigoPrincipal.ToString().Trim().Length != 0)
                                factDetalle.codigoPrincipal = detalleFactrura.codigoPrincipal.ToString().Trim();

                            if (detalleFactrura.codigoAuxiliar.ToString().Trim().Length != 0)
                                factDetalle.codigoAuxiliar = detalleFactrura.codigoAuxiliar.ToString().Trim();

                            factDetalle.descripcion = detalleFactrura.descripcion.ToString().Trim().Contains("&amp;") ? detalleFactrura.descripcion.ToString().Trim().Replace("&amp;", "") : detalleFactrura.descripcion.ToString().Trim().Contains("&") ? detalleFactrura.descripcion.ToString().Trim().Replace("&", "Y") : detalleFactrura.descripcion.ToString().Trim();
                            factDetalle.cantidad = Convert.ToInt32(detalleFactrura.cantidad.ToString().Trim());
                            factDetalle.precioUnitario = Convert.ToDecimal(detalleFactrura.precioUnitario.ToString().Trim());
                            factDetalle.descuento = Convert.ToDecimal(detalleFactrura.descuento.ToString().Trim());
                            factDetalle.precioTotalSinImpuesto = Convert.ToDecimal(detalleFactrura.precioTotalSinImpuesto.ToString().Trim());

                            #region FACTURA_DETALLE_ADICIONAL

                            if (!detalleFactrura.detalleAdicional.Equals(null))
                            {

                                if (detalleFactrura.detalleAdicional.Count > 0)
                                {
                                    facturaDetalleDetAdicional[] factDetDetalleAdicional = new facturaDetalleDetAdicional[detalleFactrura.detalleAdicional.Count];

                                    foreach (var detalleAdicional in detalleFactrura.detalleAdicional)
                                    {
                                        if (detalleAdicional.nombre.ToString().Trim().Length != 0 && detalleAdicional.valor.ToString().Trim().Length != 0)
                                        {
                                            facturaDetalleDetAdicional factDetalleAdicional = new facturaDetalleDetAdicional();
                                            if (detalleAdicional.nombre.ToString().Trim().Length != 0)
                                                factDetalleAdicional.nombre = detalleAdicional.nombre.ToString().Trim();
                                            if (detalleAdicional.valor.ToString().Trim().Length != 0)
                                                factDetalleAdicional.valor = detalleAdicional.valor.ToString().Trim().Contains("&amp;") ? detalleAdicional.valor.ToString().Trim().Replace("&amp;", "") : detalleAdicional.valor.ToString().Trim().Contains("&") ? detalleAdicional.valor.ToString().Trim().Replace("&", "Y") : detalleAdicional.valor.ToString().Trim();

                                            factDetDetalleAdicional[contFactDetImpuesto] = factDetalleAdicional;
                                            contFactDetImpuesto++;
                                        }
                                    }

                                    if (contFactDetImpuesto != 0)
                                        factDetalle.detallesAdicionales = factDetDetalleAdicional;
                                }
                                contFactDetImpuesto = 0;

                                #endregion FACTURA_DETALLE_ADICIONAL
                            }

                            #region FACTURA_DETALLE_IMPUESTO

                            if (detalleFactrura.detalleImpuesto.Count > 0)
                            {
                                impuesto_FA[] factdetalleImpuestos = new impuesto_FA[detalleFactrura.detalleImpuesto.Count];

                                foreach (var detalleImpuesto in detalleFactrura.detalleImpuesto)
                                {
                                    impuesto_FA factdetalleImpuesto = new impuesto_FA();

                                    factdetalleImpuesto.codigo = detalleImpuesto.codigo.ToString().Trim();
                                    factdetalleImpuesto.codigoPorcentaje = detalleImpuesto.codigoPorcentaje.ToString().Trim();
                                    factdetalleImpuesto.baseImponible = Convert.ToDecimal(detalleImpuesto.baseImponible.Replace(".", ",").ToString());
                                    factdetalleImpuesto.tarifa = Convert.ToDecimal(detalleImpuesto.tarifa.ToString().Trim());// alerta hay q hacer un calculo;
                                    factdetalleImpuesto.valor = Convert.ToDecimal(detalleImpuesto.valor.ToString().Trim());
                                    factdetalleImpuestos[contFactDetImpuesto] = factdetalleImpuesto;

                                    contFactDetImpuesto++;
                                }

                                factDetalle.impuestos = factdetalleImpuestos;
                            }
                            contFactDetImpuesto = 0;
                            //factDetalles[cont] = factDetalle;

                            factDetalles.Add(new facturaDetalle()
                            {
                                cantidad = factDetalle.cantidad,
                                codigoAuxiliar = factDetalle.codigoAuxiliar,
                                codigoPrincipal = factDetalle.codigoPrincipal,
                                descripcion = factDetalle.descripcion,
                                descuento = factDetalle.descuento,
                                detallesAdicionales = factDetalle.detallesAdicionales,
                                impuestos = factDetalle.impuestos,
                                precioTotalSinImpuesto = factDetalle.precioTotalSinImpuesto,
                                precioUnitario = factDetalle.precioUnitario,

                            });

                            cont++;
                            #endregion FACTURA_DETALLE_IMPUESTO
                        }
                        Factura.detalles = factDetalles;


                    }
                    cont = 0;
                    #endregion FACTURA_DETALLE

                    #region FACTURA_INFO_ADICIONAL
                    cont = 0;
                    if (cabFactura.infoAdicional.Count > 0)
                    {
                        facturaCampoAdicional[] facturaInformacionAdicional = new facturaCampoAdicional[cabFactura.infoAdicional.Count];

                        foreach (var infoAdicional in cabFactura.infoAdicional)
                        {

                            if (infoAdicional.nombre.ToString().Trim().Length != 0 && infoAdicional.valor.ToString().Trim().Length != 0)
                            {
                                facturaCampoAdicional factInfoAdicional = new facturaCampoAdicional();
                                factInfoAdicional.nombre = infoAdicional.nombre.ToString().Trim();
                                factInfoAdicional.Value = infoAdicional.valor.ToString().Trim().Contains("&amp;") ? infoAdicional.valor.ToString().Trim().Replace("&amp;", "") : infoAdicional.valor.ToString().Trim().Contains("&") ? infoAdicional.valor.ToString().Trim().Replace("&", "Y") : infoAdicional.valor.ToString().Trim();
                                facturaInformacionAdicional[cont] = factInfoAdicional;
                                cont++;
                            }
                        }

                        if (cont != 0)
                            Factura.infoAdicional = facturaInformacionAdicional;
                    }

                    cont = 0;
                    #endregion FACTURA_INFO_ADICIONAL

                    Object obj = new Object();
                    obj = Factura;
                    xmlGereration = new
                    {
                        Xml = Serializacion.serializar(obj),
                        claveAcceso = Factura.infoTributaria.claveAcceso
                    };

                    return xmlGereration;

                }
                else
                {
                    xmlGereration = "Error";
                    return xmlGereration;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
