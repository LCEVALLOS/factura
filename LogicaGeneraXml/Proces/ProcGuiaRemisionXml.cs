﻿using LogicaGeneraXml.Serialitation;
using LogicaGeneraXml.Utilitarios;
using System;

namespace LogicaGeneraXml.Proces
{
    public class ProcGuiaRemisionXml
    {
        private readonly ClaveAcceso _claveAcceso = new ClaveAcceso();

        public dynamic GuiaRemisionXml(dynamic DataRowGuiaRemision, string Version, string codDoc,string ruc, string razonSocial, string nombreComercial, string dirMatriz, string dirEstablecimiento, string contribuyenteEspecial, string obligadoContabilidad)
        {
            try
            {
                dynamic xmlGuiaRemision = null;
                if (!DataRowGuiaRemision.Equals(null))
                {
                    #region INFORMACION_TRIBUTARIA
                    guiaRemision GuiaRemision = new guiaRemision();


                    GuiaRemision.version = Version;
                    GuiaRemision.id = guiaRemisionID.comprobante;

                    infoTributaria_GR infoTributariaGR = new infoTributaria_GR();
                    infoTributariaGR.ambiente = DataRowGuiaRemision.ambiente;
                    infoTributariaGR.dirMatriz = dirMatriz;
                    infoTributariaGR.nombreComercial = nombreComercial;
                    infoTributariaGR.razonSocial =razonSocial;
                    infoTributariaGR.ruc =ruc;

                    guiaRemisionInfoGuiaRemision objInfoGuiaRemision = new guiaRemisionInfoGuiaRemision();

                    objInfoGuiaRemision.obligadoContabilidad = obligadoContabilidad;


                    #endregion INFORMACION_TRIBUTARIA
                    int cont = 0;
                    #region GuiaRemision
                    #region INFORMACION_TRIBUTARIA_
                    if (DataRowGuiaRemision.claveAcceso == "" || DataRowGuiaRemision.claveAcceso == null )
                    {
                        infoTributariaGR.claveAcceso = _claveAcceso.GenerarClaveAccesoDocumento("07",
                                                                                           DataRowGuiaRemision.secuencial.Trim(),
                                                                                           DataRowGuiaRemision.puntoEmision.Trim(),
                                                                                           DataRowGuiaRemision.establecimiento.Trim(),
                                                                                           DataRowGuiaRemision.fechaIniTransporte,
                                                                                           ruc,
                                                                                          // DataRowGuiaRemision.tipoIdentificacionTransportista.ToString().Trim(),
                                                                                           DataRowGuiaRemision.ambiente);
                    }
                    else
                    {
                        infoTributariaGR.claveAcceso = DataRowGuiaRemision.claveAcceso.Trim();  //proviene de CompRetencion
                    }
                    infoTributariaGR.estab = DataRowGuiaRemision.establecimiento.ToString();
                    infoTributariaGR.ptoEmi = DataRowGuiaRemision.puntoEmision.ToString();
                    infoTributariaGR.secuencial = DataRowGuiaRemision.secuencial.ToString();
                    infoTributariaGR.tipoEmision = DataRowGuiaRemision.tipoEmision.ToString();
                    infoTributariaGR.codDoc = codDoc;
                    #endregion INFORMACION_TRIBUTARIA_
                    GuiaRemision.infoTributaria = infoTributariaGR;

                    #region INFO_GUIA_REMISION
                    objInfoGuiaRemision.dirEstablecimiento = dirEstablecimiento;
                    objInfoGuiaRemision.dirPartida = DataRowGuiaRemision.direccionPartida.ToString();
                    objInfoGuiaRemision.razonSocialTransportista = DataRowGuiaRemision.razonSocialTransportista.ToString();
                    objInfoGuiaRemision.tipoIdentificacionTransportista = "05";// revisar  DataRowGuiaRemision.tipoIdentificacionTransportista.ToString();
                    objInfoGuiaRemision.rucTransportista = DataRowGuiaRemision.rucTransportista.ToString();

                    if (DataRowGuiaRemision.rise.ToString() != null && DataRowGuiaRemision.rise.ToString().Trim().Length > 0)
                        objInfoGuiaRemision.rise = DataRowGuiaRemision.rise.ToString();

                    objInfoGuiaRemision.fechaIniTransporte = DataRowGuiaRemision.fechaIniTransporte.ToString();
                    objInfoGuiaRemision.fechaFinTransporte = DataRowGuiaRemision.fechaFinTransporte.ToString();
                    objInfoGuiaRemision.placa = DataRowGuiaRemision.placa.ToString();
                    #endregion INFO_GUIA_REMISION
                    GuiaRemision.infoGuiaRemision = objInfoGuiaRemision;

                    #region GuiaRemision_DETALLE
                    cont = 0;
                    if (DataRowGuiaRemision.destinatario.Count > 0)
                    {
                        guiaRemisionDestinatarios objGuiaRemisionDestinatario = new guiaRemisionDestinatarios();
                        destinatario[] Destinatarios = new destinatario[DataRowGuiaRemision.destinatario.Count];
                        foreach (var DrwDestinatario in DataRowGuiaRemision.destinatario)
                        {
                            #region DESTINATARIO
                            destinatario Destinatario = new destinatario();
                            Destinatario.identificacionDestinatario = DrwDestinatario.identificacionDestinatario.ToString();
                            Destinatario.razonSocialDestinatario = DrwDestinatario.razonSocialDestinatario.ToString();
                            Destinatario.dirDestinatario = DrwDestinatario.direccionDestinatario.ToString();
                            Destinatario.motivoTraslado = DrwDestinatario.motivoTraslado.ToString();

                            if (DrwDestinatario.documentoAduaneroUnico.ToString() != null && DrwDestinatario.documentoAduaneroUnico.ToString().Trim().Length > 0)
                                Destinatario.docAduaneroUnico = DrwDestinatario.documentoAduaneroUnico.ToString();

                            if (DrwDestinatario.codigoEstablecimientoDestino.ToString() != null && DrwDestinatario.codigoEstablecimientoDestino.ToString().Trim().Length > 0)
                                Destinatario.codEstabDestino = DrwDestinatario.codigoEstablecimientoDestino.ToString();

                            if (DrwDestinatario.ruta.ToString() != null && DrwDestinatario.ruta.ToString() != "")
                                Destinatario.ruta = DrwDestinatario.ruta.ToString();

                            if (DrwDestinatario.tipoDocumentoSustento.ToString() != null && DrwDestinatario.tipoDocumentoSustento.ToString().Trim().Length > 0)
                                Destinatario.codDocSustento = DrwDestinatario.tipoDocumentoSustento.ToString();

                            if (DrwDestinatario.numeroDocumentoSustento.ToString() != null && DrwDestinatario.numeroDocumentoSustento.ToString().Trim().Length > 0)
                                Destinatario.numDocSustento = DrwDestinatario.numeroDocumentoSustento.ToString();

                            if (DrwDestinatario.numeroAutorizacionDocumentoSustento.ToString() != null && DrwDestinatario.numeroAutorizacionDocumentoSustento.ToString().Trim().Length > 0)
                                Destinatario.numAutDocSustento = DrwDestinatario.numeroAutorizacionDocumentoSustento.ToString();

                            if (DrwDestinatario.fechaEmisionDocumentoSustento.ToString() != null && DrwDestinatario.fechaEmisionDocumentoSustento.ToString().Trim().Length > 0)
                                Destinatario.fechaEmisionDocSustento = DrwDestinatario.fechaEmisionDocumentoSustento.ToString();
                            #endregion DESTINATARIO

                            #region DESTINATARIO_DETALLES

                            if (DrwDestinatario.detalle.Count > 0)
                            {
                                destinatarioDetalles DestinatarioDetalles = new destinatarioDetalles();
                                detalle[] detalles = new detalle[DrwDestinatario.detalle.Count];
                                int contdta = 0;
                                foreach (var drwDestinatarioDetalle in DrwDestinatario.detalle)
                                {
                                    detalle Detalle = new detalle();
                                    Detalle.codigoInterno = drwDestinatarioDetalle.codigoInterno.ToString();

                                    if (drwDestinatarioDetalle.codigoAdicional.ToString() != null && drwDestinatarioDetalle.codigoAdicional.ToString().Trim().Length > 0)
                                        Detalle.codigoAdicional = drwDestinatarioDetalle.codigoAdicional.ToString();

                                    Detalle.descripcion = drwDestinatarioDetalle.descripcion.ToString();
                                    
                                    try
                                    {
                                        Detalle.cantidad = Convert.ToDecimal(drwDestinatarioDetalle.cantidad.ToString());
                                    }
                                    catch
                                    {
                                        Detalle.cantidad = Convert.ToDecimal("0.00");
                                    }
                                    

                                    #endregion CAPTURO_DETALLES_ADICIONALES
                                    if (drwDestinatarioDetalle.adicional.Count > 0)
                                    {
                                        detalleDetAdicional[] detallesAdicionales = new detalleDetAdicional[drwDestinatarioDetalle.adicional.Count];
                                        int contdtaad = 0;
                                        foreach (var drwDetAdicional in drwDestinatarioDetalle.adicional)
                                        {
                                           
                                                detalleDetAdicional DetAdicional = new detalleDetAdicional();
                                                DetAdicional.nombre = drwDetAdicional.nombre.ToString();
                                                try
                                                {
                                                    DetAdicional.valor = drwDetAdicional.valor.ToString();
                                                }
                                                catch
                                                {

                                                    DetAdicional.valor = "0.00";
                                                }
                                               

                                            detallesAdicionales[contdtaad] = DetAdicional;
                                            contdtaad++;
                                        }
                                        Detalle.detallesAdicionales = detallesAdicionales;
                                    }
                                    detalles[contdta] = Detalle;
                                    contdta++;
                                }
                                DestinatarioDetalles.detalle = detalles;
                                Destinatario.detalles = DestinatarioDetalles;
                            }
                            #endregion DESTINATARIO_DETALLES
                            Destinatarios[cont] = Destinatario;
                            cont++;
                        }
                        cont = 0;
                        objGuiaRemisionDestinatario.destinatario = Destinatarios;
                        GuiaRemision.destinatarios = objGuiaRemisionDestinatario;
                    }
                    #endregion GuiaRemision_DETALLE

                    #region GUIA_REMISION_INFO_ADICIONAL
                    cont = 0;
                    if (DataRowGuiaRemision.infoAdicional.Count > 0)
                    {
                        guiaRemisionCampoAdicional[] GuiaRemisionInformacionAdicional = new guiaRemisionCampoAdicional[DataRowGuiaRemision.infoAdicional.Count];

                        foreach (var DataRowFactInfoAdicional in DataRowGuiaRemision.infoAdicional)
                        {
                            guiaRemisionCampoAdicional InfoAdicional = new guiaRemisionCampoAdicional();
                            InfoAdicional.nombre = DataRowFactInfoAdicional.nombre.ToString();
                            InfoAdicional.Value = DataRowFactInfoAdicional.valor.ToString();
                            GuiaRemisionInformacionAdicional[cont] = InfoAdicional;
                            cont++;
                        }
                        GuiaRemision.infoAdicional = GuiaRemisionInformacionAdicional;
                    }
                    cont = 0;
                    #endregion GUIA_REMISION_INFO_ADICIONAL

                    Object obj = new Object();
                    obj = GuiaRemision;
                    xmlGuiaRemision = new { Xml = Serializacion.serializar(GuiaRemision), claveAcceso = GuiaRemision.infoTributaria.claveAcceso };
                    
                }
                return xmlGuiaRemision;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
