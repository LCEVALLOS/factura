﻿using LogicaGeneraXml.Serialitation;
using LogicaGeneraXml.Utilitarios;
using System;

namespace LogicaGeneraXml.Proces
{
    public class ProcNotaCreditoXml
    {
        private readonly ClaveAcceso _claveAcceso = new ClaveAcceso();
        public dynamic NotaCreditoXml(dynamic DataRowNotaCredito, string Version, string codDoc, string ruc, string razonSocial, string nombreComercial, string dirMatriz, string dirEstablecimiento, string contribuyenteEspecial, string obligadoContabilidad)
        {
            try
            {
                dynamic xmlNotaCredito = null;

                if (DataRowNotaCredito != null)
                {
                    #region INFORMACION_TRIBUTARIA
                    notaCredito NotaCredito = new notaCredito();
                    NotaCredito.id = notaCreditoID.comprobante;
                    NotaCredito.version = Version;

                    infoTributaria_NC infoTributariaNC = new infoTributaria_NC();

                    infoTributariaNC.nombreComercial = nombreComercial;  //proviene de Compania 
                    infoTributariaNC.razonSocial = razonSocial;  //proviene de Compania
                    infoTributariaNC.ruc = ruc;   //proviene de Compania
                    infoTributariaNC.ambiente = DataRowNotaCredito.ambiente;
                    infoTributariaNC.dirMatriz = dirMatriz; //proviene de Compania

                    notaCreditoInfoNotaCredito NotaCreditoInfoNotaCredito = new notaCreditoInfoNotaCredito();
                  
                    #endregion INFORMACION_TRIBUTARIA


                    try
                    {
                        #region NotaCredito
                        if (DataRowNotaCredito.claveAcceso == "" || DataRowNotaCredito.claveAcceso == null)
                        {
                            infoTributariaNC.claveAcceso= _claveAcceso.GenerarClaveAccesoDocumento("04",
                                                                                           DataRowNotaCredito.secuencial.Trim(),
                                                                                           DataRowNotaCredito.puntoEmision.Trim(),
                                                                                           DataRowNotaCredito.establecimiento.Trim(),
                                                                                           DataRowNotaCredito.fechaEmision.Trim(),
                                                                                           ruc,
                                                                                           DataRowNotaCredito.ambiente);

                        }
                        else { infoTributariaNC.claveAcceso = DataRowNotaCredito.claveAcceso.ToString().Trim(); }
                        infoTributariaNC.estab = DataRowNotaCredito.establecimiento.ToString().Trim();
                        infoTributariaNC.ptoEmi = DataRowNotaCredito.puntoEmision.ToString().Trim();
                        infoTributariaNC.secuencial = DataRowNotaCredito.secuencial.ToString().Trim();
                        infoTributariaNC.tipoEmision = DataRowNotaCredito.tipoEmision.ToString().Trim();
                        infoTributariaNC.codDoc = codDoc;
                        NotaCredito.infoTributaria = infoTributariaNC;

                        NotaCreditoInfoNotaCredito.fechaEmision = DataRowNotaCredito.fechaEmision.ToString().Trim();
                        NotaCreditoInfoNotaCredito.dirEstablecimiento = dirEstablecimiento;//REVISAR

                        NotaCreditoInfoNotaCredito.tipoIdentificacionComprador = DataRowNotaCredito.tipoIdentificacionComprador.ToString().Trim();
                        NotaCreditoInfoNotaCredito.razonSocialComprador = DataRowNotaCredito.razonSocialComprador.ToString().Trim();
                        NotaCreditoInfoNotaCredito.identificacionComprador = DataRowNotaCredito.identificacionComprador.ToString().Trim();

                        NotaCreditoInfoNotaCredito.totalSinImpuestos = Convert.ToDecimal(DataRowNotaCredito.totalSinImpuestos.ToString());
                        if (DataRowNotaCredito.rise.ToString().Trim().Length != 0)
                            NotaCreditoInfoNotaCredito.rise = DataRowNotaCredito.rise.ToString().Trim();

                        NotaCreditoInfoNotaCredito.codDocModificado = DataRowNotaCredito.tipoDocumentoModificado.ToString();
                        NotaCreditoInfoNotaCredito.numDocModificado = DataRowNotaCredito.numeroDocumentoModificado.ToString().Trim();
                        NotaCreditoInfoNotaCredito.fechaEmisionDocSustento = DataRowNotaCredito.fechaEmisionDocumentoModificado.ToString();
                        NotaCreditoInfoNotaCredito.totalSinImpuestos = Convert.ToDecimal(DataRowNotaCredito.totalSinImpuestos.ToString());
                        NotaCreditoInfoNotaCredito.valorModificacion = Convert.ToDecimal(DataRowNotaCredito.valorModificacion.ToString());
                        NotaCreditoInfoNotaCredito.moneda = DataRowNotaCredito.moneda.ToString().Trim();
                        NotaCreditoInfoNotaCredito.motivo = DataRowNotaCredito.motivo.ToString().Trim();
                        NotaCredito.infoNotaCredito = NotaCreditoInfoNotaCredito;

                        #region NotaCredito_TOTAL_IMPUESTO
                        int cont = 0;
                        if (DataRowNotaCredito.totalImpuesto.Count > 0)
                        {
                            totalConImpuestosTotalImpuesto[] TotalImpuestos = new totalConImpuestosTotalImpuesto[DataRowNotaCredito.totalImpuesto.Count];
                            foreach (var DataRowFactTotalImpuesto in DataRowNotaCredito.totalImpuesto)
                            {
                                totalConImpuestosTotalImpuesto TotalImpuesto = new totalConImpuestosTotalImpuesto();
                                TotalImpuesto.codigo = DataRowFactTotalImpuesto.codigo.ToString();
                                TotalImpuesto.codigoPorcentaje = DataRowFactTotalImpuesto.codigoPorcentaje.ToString();
                                TotalImpuesto.baseImponible = Convert.ToDecimal(DataRowFactTotalImpuesto.baseImponible.ToString());
                                TotalImpuesto.valor = Convert.ToDecimal(DataRowFactTotalImpuesto.valor.ToString());
                                TotalImpuestos[cont] = TotalImpuesto;
                                cont++;
                            }
                            NotaCredito.infoNotaCredito.totalConImpuestos = TotalImpuestos;
                        }
                        cont = 0;
                        #endregion NotaCredito_TOTAL_IMPUESTO

                        #region NotaCredito_DETALLE
                        cont = 0;
                        if (DataRowNotaCredito.detalle.Count > 0)
                        {
                            notaCreditoDetalle[] Detalles = new notaCreditoDetalle[DataRowNotaCredito.detalle.Count];
                            foreach (var DataRowDetalle in DataRowNotaCredito.detalle)
                            {
                                notaCreditoDetalle Detalle = new notaCreditoDetalle();
                                Detalle.descuentoSpecified = true;
                                Detalle.codigoInterno = DataRowDetalle.codigoInterno.ToString();
                                if (DataRowDetalle.codigoAdicional.ToString().Trim().Length != 0)
                                    Detalle.codigoAdicional = DataRowDetalle.codigoAdicional.ToString();
                                Detalle.descripcion = DataRowDetalle.descripcion.ToString();
                                Detalle.cantidad = Convert.ToInt32(DataRowDetalle.cantidad.ToString());
                                Detalle.precioUnitario = Convert.ToDecimal(DataRowDetalle.precioUnitario.ToString());
                                Detalle.descuento = Convert.ToDecimal(DataRowDetalle.descuento.ToString());
                                Detalle.precioTotalSinImpuesto = Convert.ToDecimal(DataRowDetalle.precioTotalSinImpuesto.ToString());

                                if (!DataRowDetalle.Equals(null))
                                {

                                    #region NOTA_CREDITO_DETALLE_ADICIONAL

                                    if (DataRowDetalle.detalleAdicional.Count > 0)
                                    {
                                        notaCreditoDetalleDetAdicional[] DetDetalleAdicional = new notaCreditoDetalleDetAdicional[DataRowDetalle.detalleAdicional.Count];
                                        int indexdetallenc = 0;
                                        foreach (var DataRowDetalleAdicional in DataRowDetalle.detalleAdicional)
                                        {
                                            notaCreditoDetalleDetAdicional DetalleAdicional = new notaCreditoDetalleDetAdicional();
                                            DetalleAdicional.nombre = DataRowDetalleAdicional.nombre.ToString();
                                            DetalleAdicional.valor = DataRowDetalleAdicional.valor.ToString();

                                            DetDetalleAdicional[indexdetallenc] = DetalleAdicional;
                                            indexdetallenc++;
                                        }
                                        Detalle.detallesAdicionales = DetDetalleAdicional;
                                    }
                                    #endregion NOTA_CREDITO_DETALLE_ADICIONAL

                                    #region NotaCredito_DETALLE_IMPUESTO
                                   
                                    if (DataRowDetalle.detalleImpuesto.Count > 0)
                                    {
                                        impuesto_NC[] detalleImpuestos = new impuesto_NC[DataRowDetalle.detalleImpuesto.Count];
                                        int detalleImpuestNc = 0;
                                        foreach (var DataRowDetalleImpuesto in DataRowDetalle.detalleImpuesto)
                                        {
                                            impuesto_NC detalleImpuesto = new impuesto_NC();
                                            detalleImpuesto.codigo = DataRowDetalleImpuesto.codigo.ToString();
                                            detalleImpuesto.codigoPorcentaje = DataRowDetalleImpuesto.codigoPorcentaje.ToString();
                                            detalleImpuesto.baseImponible = Convert.ToDecimal(DataRowDetalleImpuesto.baseImponible.ToString());
                                            if (DataRowDetalleImpuesto.tarifa.ToString().Trim() != "")
                                            {
                                                detalleImpuesto.tarifaSpecified = true;
                                                detalleImpuesto.tarifa = Convert.ToDecimal(DataRowDetalleImpuesto.tarifa.ToString());
                                            }
                                            else
                                            {
                                                detalleImpuesto.tarifaSpecified = false;
                                            }
                                            detalleImpuesto.valor = Convert.ToDecimal(DataRowDetalleImpuesto.valor.ToString());

                                            detalleImpuestos[detalleImpuestNc] = detalleImpuesto;
                                            detalleImpuestNc++;
                                        }
                                        Detalle.impuestos = detalleImpuestos;
                                    }
                                    #endregion NotaCredito_DETALLE_IMPUESTO
                                }
                                Detalles[cont] = Detalle;
                                cont++;
                            }
                            NotaCredito.detalles = Detalles;
                        }
                        cont = 0;
                        #endregion NotaCredito_DETALLE

                        #region NotaCredito_INFO_ADICIONAL

                        cont = 0;
                        if (DataRowNotaCredito.infoAdicional.Count > 0)
                        {
                            notaCreditoCampoAdicional[] NotaCreditoInformacionAdicional = new notaCreditoCampoAdicional[DataRowNotaCredito.infoAdicional.Count];
                            foreach (var DataRowFactInfoAdicional in DataRowNotaCredito.infoAdicional)
                            {
                                notaCreditoCampoAdicional InfoAdicional = new notaCreditoCampoAdicional();
                                InfoAdicional.nombre = DataRowFactInfoAdicional.nombre.ToString();
                                InfoAdicional.Value = DataRowFactInfoAdicional.valor.ToString();
                                NotaCreditoInformacionAdicional[cont] = InfoAdicional;
                                cont++;
                            }
                            NotaCredito.infoAdicional = NotaCreditoInformacionAdicional;
                        }
                        cont = 0;
                        #endregion NotaCredito_INFO_ADICIONAL
                        #endregion NotaCredito

                        Object obj = new Object();
                        obj = NotaCredito;
                        xmlNotaCredito = new { Xml = Serializacion.serializar(obj), claveAcceso = NotaCredito.infoTributaria.claveAcceso };

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                return xmlNotaCredito;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
