﻿using System;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace LogicaGeneraXml.Serialitation
{
    public class Serializacion
    {
        public static string serializar(Object obj)
        {
            string resultXml = "";
            try
            {
                System.Xml.Serialization.XmlSerializer xml = new System.Xml.Serialization.XmlSerializer(obj.GetType());
                System.IO.MemoryStream ms = new System.IO.MemoryStream();
                var xns = new XmlSerializerNamespaces();
                xns.Add(string.Empty, string.Empty);
                //xns.Add("xsd","http://www.w3.org/2001/XMLSchema");
                //xns.Add("ds", "http://www.w3.org/2000/09/xmldsig#");

                xml.Serialize(ms, obj, xns);
                resultXml = Encoding.UTF8.GetString(ms.ToArray());

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.InnerXml = resultXml;
                xmlDoc.LoadXml(resultXml) ;

                XmlDeclaration xmlDeclaration = (XmlDeclaration)xmlDoc.FirstChild;
                xmlDeclaration.Encoding = "UTF-8";
                resultXml = xmlDoc.InnerXml;
            }
            catch (Exception ex)
            {
                resultXml = "";
            }
            return resultXml;
        }
    }
}
