﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaGeneraXml.Utilitarios
{
    public class Util
    {
        public string CalculaDigitoVerificador(string txClaveAcceso)
        {
            int digitoVerificador = -1;
            try
            {
                char[] arrDigitosRuc = txClaveAcceso.ToCharArray();
                int[] arrResultado = new int[0];
                int cont = 2;
                decimal resultado = 0;

                for (int i = (txClaveAcceso.Length - 1); i >= 0; i--)
                {

                    int[] temp = new int[(txClaveAcceso.Length) - i];
                    if (arrResultado != null)
                        Array.Copy(arrResultado, temp, Math.Min(arrResultado.Length, temp.Length));
                    arrResultado = temp;

                    arrResultado[arrResultado.Length - 1] = Convert.ToInt32(arrDigitosRuc[i].ToString()) * cont;
                    cont += 1;
                    if (cont == 8)
                    {
                        cont = 2;
                    }
                }
                resultado = arrResultado.Sum() % 11;
                resultado = 11 - resultado;

                switch (Convert.ToInt32(resultado))
                {
                    case 10:
                        digitoVerificador = 1;
                        break;
                    case 11:
                        digitoVerificador = 0;
                        break;
                    default:
                        digitoVerificador = Convert.ToInt32(resultado);
                        break;
                }
            }
            catch (Exception ex)
            {

            }
            return digitoVerificador.ToString();
        }
    }
}
