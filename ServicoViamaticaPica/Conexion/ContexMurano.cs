﻿using Newtonsoft.Json;
using RestSharp;
using ServicoViamaticaPica.Modal;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ServicoViamaticaPica.Conexion
{
    class ContexMurano
    {
        public List<Data> ConsultaMurano()
        {
            try
            {
                 var metodo = ConfigurationManager.AppSettings["metodo"].ToString();
                var cod_table = ConfigurationManager.AppSettings["codTable"].ToString();
                var urlTblConf = ConfigurationManager.AppSettings["UrlMurano"].ToString() + metodo + "=" + cod_table;

                var client = new RestClient(urlTblConf);
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);

                if (response.StatusDescription.Equals("Not Found"))
                {
                    throw new Exception();
                }

                resurl confitb = JsonConvert.DeserializeObject<resurl>(response.Content);

                var json = JsonConvert.SerializeObject(confitb.registros.data);

                List<Data> tbconfi = JsonConvert.DeserializeObject<List<Data>>(json);

                return tbconfi;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public FacturaJson.Root GetJsonFactura(string Url)
        {
            try
            {
                dynamic res = null;
                var client = new RestClient(Url);
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                res = response.Content;
                FacturaJson.Root facJson = JsonConvert.DeserializeObject<FacturaJson.Root>(res);
                return facJson;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
        public dynamic GetJsonNotaCredito(string Url)
        {
            try
            {
                dynamic res = null;
                var client = new RestClient(Url);
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                res = response.Content;
                NotaCreditoJson.Application notCreditoJson = JsonConvert.DeserializeObject<NotaCreditoJson.Application>(res);
                return notCreditoJson;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public GuiaRemisionJson.Root GetJsonGuiaRemision(string Url)
        {
            try
            {
                dynamic res = null;
                var client = new RestClient(Url);
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                res = response.Content;
                GuiaRemisionJson.Root guiaRemisionJson = JsonConvert.DeserializeObject<GuiaRemisionJson.Root>(res);
                return guiaRemisionJson;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public dynamic GetJsonClavesAcceso()
        {
            try
            {
                dynamic res = null;
                var client = new RestClient("http://130.26.1.47/murano_produccion/murano_ws/public/puntoventa/transaccpuntoventajson/listarfacturasinautorizar");
                client.Timeout = -1;
                List<ClavesAcceso> Claves = new List<ClavesAcceso>();
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                var body = @"{
                            " + "\n" +
                              @"    ""cod_empresa"": ""1000""
                            " + "\n" +
                              @"}";
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                res = response.Content;
                

                var clave_acceso = JsonConvert.DeserializeObject(res);
                if (clave_acceso != null)
                {
                    foreach (var item in clave_acceso)
                    {
                        string cl = item["clave_acceso"];
                        ClavesAcceso claveA = new ClavesAcceso(cl);
                        Claves.Add(claveA);

                        
                    }
                }
 
                res = Claves;
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}
