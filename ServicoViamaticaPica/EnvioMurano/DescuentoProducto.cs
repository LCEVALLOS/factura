﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoViamaticaPica.EnvioMurano
{
    public class DescuentoProducto
    {
        public string EnvioDesdcuentoProducto(string descuentos)
        {
            var res = string.Empty;
            var client = new RestClient("http://130.26.1.47/murano_preproduccion/murano_ws/public/puntoventa/transaccpuntoventajson/enviodescuentoproductos");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("text/plain", descuentos, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);
            if (response.Content.Equals(""))
            {
                res = response.ErrorException.Message;
            }
            else
            {
                res = response.Content;
            }


            return res;
        }
    }
}
