﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoViamaticaPica.EnvioMurano
{
    public class GetMurano
    {
        public string RespuestaMurano(string Url)
        {
       
            try
            {
                string res = string.Empty;
                var client = new RestClient(Url);
                var request = new RestRequest(Method.GET);
                //IRestResponse response = client.Execute(request);
                //res = response.Content;
                res = "Proceso ejecutado correctamente.";
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public string RespuestaClasificacion(string Url)
        {

            try
            {
                string res = string.Empty;
                var client = new RestClient(Url);
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                res = response.Content;
               
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
