﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoViamaticaPica.EnvioMurano
{
    public class PostMurano
    {
        public string EnvioPostMurano(string url,string data)
        {
            try
            {
                var res = string.Empty;
                var client = new RestClient(url);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("text/plain", data, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                Console.WriteLine(response.Content);
                if (response.Content.Equals(""))
                {
                    res = response.ErrorException.Message;
                }
                else
                {
                    res = response.Content;
                }


                return res;

            }
            catch (Exception ex)
            {

                throw ex;
            }
          

        }
    }
}
