﻿using Newtonsoft.Json;
using RestSharp;
using ServicoViamaticaPica.Modal;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoViamaticaPica.LogicaFacturaElec
{
    public class ConsultaDocElec
    {
        public string GetDocElec(string claveAcceso)
        {
            try
            {
                var res = string.Empty;
                var UrlSoap = ConfigurationManager.AppSettings["UrlConsultafactura"];
                var idSuscriptor = ConfigurationManager.AppSettings["idSuscriptor"];
                var token = ConfigurationManager.AppSettings["token"];
                string estado = string.Empty;
                var client = new RestClient(UrlSoap);
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddParameter("idSuscriptor", idSuscriptor);
                request.AddParameter("claveAcceso", claveAcceso);
                request.AddParameter("token", token);
                IRestResponse response = client.Execute(request);

                if (response.Content.Equals(""))
                {
                    res = response.ErrorMessage;
                }
                else
                {
                    res = response.Content;

                  
                }
                return res;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
