﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using ServicoViamaticaPica.Modal;
using System;
using System.Configuration;
using System.IO;

namespace ServicoViamaticaPica.LogicaFacturaElec
{
    public class EnvioDocElec
    {
        public string PostDocElec(string xmlDoc, string claveAcceso)
        {
            try
            {
                var res = string.Empty;
                var UrlSoap = ConfigurationManager.AppSettings["UrlSoap"];
                var idSuscriptor = ConfigurationManager.AppSettings["idSuscriptor"];
                var token = ConfigurationManager.AppSettings["token"];

                var client = new RestClient(UrlSoap);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddParameter("idSuscriptor", idSuscriptor);
                request.AddParameter("claveAcceso", claveAcceso);
                request.AddParameter("token", token);
                request.AddParameter("xml", xmlDoc);
                IRestResponse response = client.Execute(request);

                if (response.Content.Equals(""))
                {
                    res = response.ErrorException.Message;
                }
                else
                {
                    res = response.Content;
                }

              
                return res;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
