﻿using RestSharp;
using ServicoViamaticaPica.Modal;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoViamaticaPica.LogicaFacturaElec
{
    public class EnvioRespuestaMurano
    {
        public string PostMuranoRespuesta(string claves)
        {
            try
            {
                var res = string.Empty;
                var UrlMuranorespuesta = ConfigurationManager.AppSettings["UrlMuranoRespuesta"];

                var client = new RestClient(UrlMuranorespuesta);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
              
                request.AddParameter("application/json", claves, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                Console.WriteLine(response.Content);

                if (response.Content.Equals(""))
                {
                    res = response.ErrorException.Message;
                }
                else
                {
                    res = response.Content;
                }


                return res;


            }
            catch (Exception ex )
            {

                throw ex;
            }

        }
    }
}
