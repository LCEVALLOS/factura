﻿using LogicaGeneraXml.Proces;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ServicoViamaticaPica.Conexion;
using ServicoViamaticaPica.Modal;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using static System.Net.Mime.MediaTypeNames;

namespace ServicoViamaticaPica.LogicaFacturaElec
{
    public class ProcesFacElec
    {
        private readonly EnvioDocElec _envioDocElec = new EnvioDocElec();
        private readonly ConsultaDocElec _consultaDocElec = new ConsultaDocElec();
        private readonly ContexMurano _contexMurano = new ContexMurano();
        private readonly EnvioRespuestaMurano _envioREspuestaMurano = new EnvioRespuestaMurano();

        private readonly string version = ConfigurationManager.AppSettings["VersionXml"];
        private readonly string ruc = ConfigurationManager.AppSettings["rucPicca"];
        private readonly string razonSocial = ConfigurationManager.AppSettings["razonSocial"];
        private readonly string nombreComercial = ConfigurationManager.AppSettings["nombreComercial"];
        private readonly string dirMatriz = ConfigurationManager.AppSettings["dirMatriz"];
        private readonly string dirEstablecimientoGye = ConfigurationManager.AppSettings["dirEstablecimientoGye"];
        private readonly string dirEstablecimientoQui = ConfigurationManager.AppSettings["dirEstablecimientoQui"];
        private readonly string dirEstablecimientoCuen = ConfigurationManager.AppSettings["dirEstablecimientoCuen"];
        private readonly string contribuyenteEspecial = ConfigurationManager.AppSettings["contribuyenteEspecial"];
        private readonly string obligadoContabilidad = ConfigurationManager.AppSettings["obligadoContabilidad"];



        public void FacturacionElectronica(List<Data> data)
        {
            try
            {

                // _consultaDocElec.GetDocElec("0710202101099000124300120270310000001306262214413");
          Factura(data.Where(x => x.descripcion.Contains("url_webservice_pica_fac")).Select(x => x.codigo_adicional1).FirstOrDefault());
     //         NotaCredito(data.Where(x => x.descripcion.Contains("url_webservice_pica_nc")).Select(x => x.codigo_adicional1).FirstOrDefault());
                // GuiaRemision(data.Where(x => x.descripcion.Contains("url_webservice_pica_gr")).Select(x => x.codigo_adicional1).FirstOrDefault());
                ClavesAcceso();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string Factura(string urlFactrura)
        {

            try
            {
                ProcFacturaXml procfacturaXml = new ProcFacturaXml();
                ArrayList respuestas = new ArrayList();
                List<RespuestaJson> acceso = new List<RespuestaJson>();
                List<RespuestaJson> error = new List<RespuestaJson>();
                var json = string.Empty;

                var res = string.Empty;

                dynamic facJson = _contexMurano.GetJsonFactura(urlFactrura);
                if (facJson != null)
                {

                    foreach (var factrura in facJson._request)
                    {
                        string dirEstablecimiento = string.Empty;
                        if (factrura.codSucursal == "80") { dirEstablecimiento = dirEstablecimientoGye; }
                        else if (factrura.codSucursal == "82") { dirEstablecimiento = dirEstablecimientoQui; }
                        else if (factrura.codSucursal == "84") { dirEstablecimiento = dirEstablecimientoCuen; }
                        xmlRes procesXml = JsonConvert.DeserializeObject<xmlRes>(JsonConvert.SerializeObject(procfacturaXml.FacturaXml(factrura, version, ConfigurationManager.AppSettings["docFactura"].ToString(), ruc, razonSocial, nombreComercial, dirMatriz, dirEstablecimiento, contribuyenteEspecial, obligadoContabilidad)));

                        res = _envioDocElec.PostDocElec(procesXml.Xml, procesXml.claveAcceso);

                        if (res.Contains("OPERACION EXITOSA"))
                        {
                            RespuestaJson claveA = new RespuestaJson(procesXml.claveAcceso, "N");
                            acceso.Add(claveA);
                            respuestas.Add(claveA);
                        }
                        else
                        {
                            RespuestaJson claveA = new RespuestaJson(procesXml.claveAcceso, factrura.codigoNumerico);
                            error.Add(claveA);
                        }

                    }
                    StreamWriter clave = File.AppendText("LogFacturaError.txt");
                    var logError = string.Empty;

                    logError = JsonConvert.SerializeObject(error);
                    clave.WriteLine(logError);
                    clave.Close();


                    json = JsonConvert.SerializeObject(acceso);
                    var envioMurano = _envioREspuestaMurano.PostMuranoRespuesta(json);
                }
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string NotaCredito(string Url)
        {
            try
            {
                ArrayList respuestas = new ArrayList();
                List<RespuestaJson> acceso = new List<RespuestaJson>();
                List<RespuestaJson> error = new List<RespuestaJson>();
                ProcNotaCreditoXml _procNotaCreditoXml = new ProcNotaCreditoXml();

                var json = string.Empty;
                var res = string.Empty;
                dynamic notaCreJson = _contexMurano.GetJsonNotaCredito(Url);

                if (notaCreJson != null)
                {

                    foreach (var notaCredito in notaCreJson._request)
                    {

                        string dirEstablecimiento = string.Empty;

                        if (notaCredito.codSucursal == "80") { dirEstablecimiento = dirEstablecimientoGye; }

                        else if (notaCredito.codSucursal == "82") { dirEstablecimiento = dirEstablecimientoQui; }

                        else if (notaCredito.codSucursal == "84") { dirEstablecimiento = dirEstablecimientoCuen; }

                        xmlRes procesXml = JsonConvert.DeserializeObject<xmlRes>(JsonConvert.SerializeObject(_procNotaCreditoXml.NotaCreditoXml(notaCredito, version, ConfigurationManager.AppSettings["docNotaCredito"].ToString(), ruc, razonSocial, nombreComercial, dirMatriz, dirEstablecimiento, contribuyenteEspecial, obligadoContabilidad)));

                        res =  _envioDocElec.PostDocElec(procesXml.Xml, procesXml.claveAcceso);

                        if (res.Contains("OPERACION EXITOSA"))
                        {
                            RespuestaJson claveA = new RespuestaJson(procesXml.claveAcceso, "N");
                            acceso.Add(claveA);
                            respuestas.Add(claveA);
                        }
                        else
                        {
                            RespuestaJson claveA = new RespuestaJson(procesXml.claveAcceso, notaCredito.codigoNumerico);
                            error.Add(claveA);
                        }
                    }
                    StreamWriter clave = File.AppendText("LogNotaCreditoError.txt");
                    var logError = string.Empty;

                    logError = JsonConvert.SerializeObject(error);
                    clave.WriteLine(DateTime.Now);
                    clave.WriteLine(logError);
                    clave.Close();

                    json = JsonConvert.SerializeObject(acceso);
                     var envioMurano = _envioREspuestaMurano.PostMuranoRespuesta(json);

                }


                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string GuiaRemision(string Url)
        {
            try
            {
                ArrayList respuestas = new ArrayList();
                List<RespuestaJson> acceso = new List<RespuestaJson>();
                List<RespuestaJson> error = new List<RespuestaJson>();

                ProcGuiaRemisionXml _procGuiaRemisionXml = new ProcGuiaRemisionXml();
                var json = string.Empty;
                var res = string.Empty;

                dynamic guiaRemisionJson = _contexMurano.GetJsonGuiaRemision(Url);

                if (guiaRemisionJson != null)
                {
                    foreach (var guiaremision in guiaRemisionJson._request)
                    {
                        string dirEstablecimiento = string.Empty;

                        if (guiaremision.codSucursal == "80") { dirEstablecimiento = dirEstablecimientoGye; }

                        else if (guiaremision.codSucursal == "82") { dirEstablecimiento = dirEstablecimientoQui; }

                        else if (guiaremision.codSucursal == "84") { dirEstablecimiento = dirEstablecimientoCuen; }

                        xmlRes procesXml = JsonConvert.DeserializeObject<xmlRes>(JsonConvert.SerializeObject(_procGuiaRemisionXml.GuiaRemisionXml(guiaremision, version, ConfigurationManager.AppSettings["docGuiaRemision"].ToString(), ruc, razonSocial, nombreComercial, dirMatriz, dirEstablecimiento, contribuyenteEspecial, obligadoContabilidad)));

                        res = _envioDocElec.PostDocElec(procesXml.Xml, procesXml.claveAcceso);

                        if (res.Contains("OPERACION EXITOSA"))
                        {
                            RespuestaJson claveA = new RespuestaJson(guiaremision.codigoNumerico, "N");
                            acceso.Add(claveA);
                            respuestas.Add(claveA);

                        }
                        else
                        {
                            RespuestaJson claveA = new RespuestaJson(guiaremision.codigoNumerico + " " + res + " " + DateTime.Now.ToString("dd-MM-yyyy"));
                            error.Add(claveA);
                        }
                    }

                    StreamWriter clave = File.AppendText("LogGuiaRemisionError.txt");
                    var logError = string.Empty;

                    logError = JsonConvert.SerializeObject(error);
                    clave.WriteLine(DateTime.Now);
                    clave.WriteLine(logError);
                    clave.Close();

                    json = JsonConvert.SerializeObject(acceso);
                    var envioMurano = _envioREspuestaMurano.PostMuranoRespuesta(json);

                    return res;
                }
                return res;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private string ClavesAcceso()
        {
            try
            {
                ArrayList respuestas = new ArrayList();

                List<RespuestaJson> Exito = new List<RespuestaJson>();
                List<RespuestaJson> Limite = new List<RespuestaJson>();
                List<RespuestaJson> noComprobante = new List<RespuestaJson>();
                List<RespuestaJson> secuencial = new List<RespuestaJson>();
                List<RespuestaJson> desconocido = new List<RespuestaJson>();
                List<RespuestaJson> error = new List<RespuestaJson>();

                var json = string.Empty;
                var res = string.Empty;

                dynamic clavesNoAutorizdas = _contexMurano.GetJsonClavesAcceso();

                if (clavesNoAutorizdas.Count > 0)
                {
                    foreach (var claveNo in clavesNoAutorizdas)

                    {
                        res = _consultaDocElec.GetDocElec(claveNo.clave_acceso);

                        if (res.Contains("NO AUTORIZADO"))
                        {
                            RespuestaJson claveA = new RespuestaJson(claveNo.clave_acceso, "D");
                            error.Add(claveA);
                            respuestas.Add(claveA);

                        }
                        else if (res.Contains("LIMITE DE INTENTOS NO AUTORIZADOS POR DIA"))
                        {
                            RespuestaJson claveA = new RespuestaJson(claveNo.clave_acceso, "D");
                            Limite.Add(claveA);
                            respuestas.Add(claveA);
                        }
                        else if (res.Contains("AUTORIZADO"))
                        {
                            RespuestaJson claveA = new RespuestaJson(claveNo.clave_acceso, "A");
                            Exito.Add(claveA);
                        }
                        else if (res.Contains("NO SE ENCONTRO EL COMPROBANTE"))
                        {
                            RespuestaJson claveA = new RespuestaJson(claveNo.clave_acceso, "D");
                            noComprobante.Add(claveA);

                        }
                        else if (res.Contains("ERROR SECUENCIAL REGISTRADO")) {
                            RespuestaJson claveA = new RespuestaJson(claveNo.clave_acceso, "D");
                            secuencial.Add(claveA);

                        }
                        else
                        {
                            RespuestaJson claveA = new RespuestaJson(claveNo.clave_acceso, "D");
                            desconocido.Add(claveA);

                        }
                    }
                     if (Exito.Count > 0)
                    {
                        StreamWriter clave = File.AppendText("Claves autorizadas.txt");


                        json = JsonConvert.SerializeObject(Exito);
                        clave.WriteLine(DateTime.Now);
                        clave.WriteLine(json);
                        clave.Close();

                       var envioMurano = _envioREspuestaMurano.PostMuranoRespuesta(json);
                    }
                    if (error.Count > 0)
                    {
                        StreamWriter clave = File.AppendText("Claves no autorizadas.txt");
                        var logError = string.Empty;

                        logError = JsonConvert.SerializeObject(error);
                        clave.WriteLine(DateTime.Now);
                        clave.WriteLine(logError);
                        clave.Close();
                        var envioMurano = _envioREspuestaMurano.PostMuranoRespuesta(logError);

                    }
                    if (Limite.Count>0)
                    {
                        StreamWriter clave = File.AppendText("Claves no autorizadas por limite.txt");
                        var logError = string.Empty;

                        logError = JsonConvert.SerializeObject(Limite);
                        clave.WriteLine(DateTime.Now);
                        clave.WriteLine(logError);
                        clave.Close();
                        var envioMurano = _envioREspuestaMurano.PostMuranoRespuesta(logError);
                    }

                    if (noComprobante.Count > 0)
                    {
                        StreamWriter clave = File.AppendText("No se encontro comprobante.txt");
                        var logError = string.Empty;

                        var des = JsonConvert.SerializeObject(noComprobante);
                        clave.WriteLine(DateTime.Now);
                        clave.WriteLine(des);

                        clave.Close();
                        var envioMurano = _envioREspuestaMurano.PostMuranoRespuesta(logError);

                    }
                    if (secuencial.Count > 0)
                    {
                        StreamWriter clave = File.AppendText("secuencial.txt");

                        var logError = JsonConvert.SerializeObject(secuencial);
                        clave.WriteLine(DateTime.Now);
                        clave.WriteLine(logError);

                        clave.Close();
                     var envioMurano = _envioREspuestaMurano.PostMuranoRespuesta(logError);

                    }
                    if (desconocido.Count > 0)
                    {
                        StreamWriter clave = File.AppendText("Desconocido.txt");
                        var logError = string.Empty;

                        var des = JsonConvert.SerializeObject(desconocido);
                        clave.WriteLine(DateTime.Now);
                        clave.WriteLine(des);

                        clave.Close();
                        //var envioMurano = _envioREspuestaMurano.PostMuranoRespuesta(logError);

                    }


                }


                return res;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
    }
}
