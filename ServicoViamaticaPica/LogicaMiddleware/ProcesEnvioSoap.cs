﻿using LogicaGeneraXml.ModalEndpoint;
using Newtonsoft.Json;
using ServicoViamaticaPica.EnvioMurano;
using ServicoViamaticaPica.ModalEndpoint;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoViamaticaPica.LogicaMiddleware
{
    public class ProcesEnvioSoap
    {
        #region Metodos Http
        private readonly PostMurano _postMurano = new PostMurano();
        private readonly GetMurano _getMurano = new GetMurano();
        #endregion

        #region Url Post Murano
        private readonly string UrlCatalogoClientes = ConfigurationManager.AppSettings["UrlCatalogoClientes"];
        private readonly string UrlRegistrarFacturas = ConfigurationManager.AppSettings["UrlRegistrarFacturas"];
        private readonly string UrlMotivosPedidos = ConfigurationManager.AppSettings["UrlMotivosPedidos"];
        private readonly string UrlCatalogoProductos = ConfigurationManager.AppSettings["UrlCatalogoProductos"];
        private readonly string UrlDescuentoProductos = ConfigurationManager.AppSettings["UrlDescuentoProductos"];
        private readonly string UrlAnticipoReportadosPuntoVenta = ConfigurationManager.AppSettings["UrlAnticipoReportadosPuntoVenta"];
        private readonly string UrlCajaReportadosPuntoVenta = ConfigurationManager.AppSettings["UrlCajaReportadosPuntoVenta"];
        private readonly string UrlCatalogoCompensacion = ConfigurationManager.AppSettings["UrlCatalogoCompensacion"];
        private readonly string UrlDescuentoCategoria = ConfigurationManager.AppSettings["UrlDescuentoCategoria"];
        private readonly string UrlClasificacionmaterial = ConfigurationManager.AppSettings["UrlClasificacionmaterial"];
        private readonly string UrlListarPrecios = ConfigurationManager.AppSettings["UrlListarPrecios"];
        private readonly string UrlListarIngresarStockProductos = ConfigurationManager.AppSettings["UrlListarIngresarStockProductos"];

        #endregion

        string cod_empresa = "1000";
        string tipoMaterial = "ZTER";
        string bodega = string.Empty;
        public void EnvioSoap()
        {
            var fechaRegistro = string.Empty;
            var nciclo = string.Empty;
            var fechaActual = DateTime.Now.ToString("dd/MM/yyyy");
            if (File.Exists("LogFecha.txt"))
            {
                StreamReader fechaArchivo = File.OpenText("LogFecha.txt");
                 fechaRegistro = fechaArchivo.ReadLine();
                 fechaArchivo.Close();

            }
            if (File.Exists("ciclo.txt"))
            {
                TextReader cicloLeer = new StreamReader("ciclo.txt");
                 nciclo = cicloLeer.ReadLine();
                  cicloLeer.Close();
            }



            //hacer log para fechas 
            var intHoraSystema = Convert.ToInt32(DateTime.Now.ToString("H:mm").Trim().Replace(":", ""));
            if (/*intHoraSystema >= 1504 && intHoraSystema <= 1530 && */!fechaRegistro.Equals(fechaActual))
            {
               

                if (nciclo.Equals("0") || nciclo.Equals("") )
                {
                    var CatalogoCliente = ListarCatalogoCliente();
                    if (CatalogoCliente.Contains("00"))
                    {
                        var clasificacionMaterial = GetClasificacionMaterial();
                        if (clasificacionMaterial.Contains("Proceso ejecutado correctamente"))
                        {
                            var CatalogoProduto = EnvioCatalogoProductos();
                            if (CatalogoProduto.Contains("00"))
                            {
                                var ListarPrecios = GetListarPrecios();
                                if (ListarPrecios.Contains("1"))
                                {
                                    var StockProductos = ListarIngresarStockProductos();
                                    if (StockProductos.Contains("00"))
                                    {
                                        // var ListarDescuentoCategoria = Descuento(UrlDescuentoCategoria);
                                        var ListarDescuentoCategoria = Descuento("https://localhost:44382/WeatherForecast/Descuento");
                                        if (ListarDescuentoCategoria.Contains("00"))
                                        {
                                            // var EnvioDescuentoProducto = Descuento(UrlDescuentoProductos);
                                            var EnvioDescuentoProducto = Descuento("https://localhost:44382/WeatherForecast/Descuento");
                                            if (EnvioDescuentoProducto.Contains("00"))
                                            {
                                                TextWriter ciclo = new StreamWriter("ciclo.txt");
                                                StreamWriter fecha = File.AppendText("LogFecha.txt");
                                                ciclo.WriteLine("1");
                                                ciclo.Close();
                                                fecha.Write(DateTime.Now.ToString("dd/MM/yyyy"));
                                                fecha.Close();
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                }


            }
            else
            {
                TextWriter ciclo = new StreamWriter("ciclo.txt");
                ciclo.WriteLine("0");
                ciclo.Close();
            }




        }
        public string ListarCatalogoCliente()
        {
            string res = string.Empty;
            var json = string.Empty;


            int canal = 78;
            for (int i = 0; i <= 2; i++)
            {
                canal += 2;

                ModalCatalogoCliente modalCatalogoCliente = new ModalCatalogoCliente(canal.ToString(), "101");
                json = JsonConvert.SerializeObject(modalCatalogoCliente).Replace("[", "").Replace("]", "");

                // res = _postMurano.EnvioPostMurano(UrlCatalogoClientes, json);
                res = _postMurano.EnvioPostMurano("https://localhost:44382/WeatherForecast/Clientes", json);

            }


            return res;
        }

        public string GetClasificacionMaterial()
        {
            string res = string.Empty;
            res = _getMurano.RespuestaMurano(UrlClasificacionmaterial);

            return res;
        }

        public string EnvioCatalogoProductos()
        {
            string res = string.Empty;
            var json = string.Empty;

            int canal = 78;

            for (int i = 0; i <= 2; i++)
            {
                canal += 2;
                if (canal == 80)
                {
                    bodega = "1006";
                }
                else if (canal == 82)
                {
                    bodega = "1005";
                }
                else if (canal == 84)
                {
                    bodega = "1007";
                }
                ModalCatalogoProductos modalCatalogoProductos = new ModalCatalogoProductos(cod_empresa, canal.ToString(), tipoMaterial, bodega);
                json = JsonConvert.SerializeObject(modalCatalogoProductos).Replace("[", "").Replace("]", "");

                //res = _postMurano.EnvioPostMurano(UrlCatalogoProductos, json);
                res = _postMurano.EnvioPostMurano("https://localhost:44382/WeatherForecast/Productos", json);

            }

            return res;
        }
        public string GetListarPrecios()
        {
            string res = string.Empty;
            // res = _getMurano.RespuestaClasificacion(UrlListarPrecios);
            res = _getMurano.RespuestaClasificacion("https://localhost:44382/WeatherForecast/Clasificacion");

            return res;
        }
        public string ListarIngresarStockProductos()
        {
            string res = string.Empty;
            var json = string.Empty;

            int bodega = 1004;
            for (int i = 0; i <= 2; i++)
            {
                bodega += 1;

                ModalListarIngresarStockProductos modalListarIngresarStockProductos = new ModalListarIngresarStockProductos(bodega.ToString());
                json = JsonConvert.SerializeObject(modalListarIngresarStockProductos).Replace("[", "").Replace("]", "");

                // res = _postMurano.EnvioPostMurano(UrlListarIngresarStockProductos, json);
                res = _postMurano.EnvioPostMurano("https://localhost:44382/WeatherForecast/Bodega", json);

            }

            return res;
        }
        public string Descuento(string Url)
        {
            string res = string.Empty;
            var json = string.Empty;

            int canal = 78;
            for (int i = 0; i <= 2; i++)
            {
                canal += 2;

                ModalDescuento modalDescuento = new ModalDescuento(canal.ToString(), "16", "");
                json = JsonConvert.SerializeObject(modalDescuento).Replace("[", "").Replace("]", "");

                res = _postMurano.EnvioPostMurano(Url, json);

            }

            return res;
        }

    }
}
