﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoViamaticaPica.Modal
{
    public class FacturaJson 
    {
        
        public class DetalleImpuesto
        {
            public string codigoPrincipal { get; set; }
            public int codigo { get; set; }
            public string codigoPorcentaje { get; set; }
            public int tarifa { get; set; }
            public string baseImponible { get; set; }
            public double valor { get; set; }
        }

        public class DetalleAdicional
        {
            public string codigoPrincipal { get; set; }
            public string nombre { get; set; }
            public string valor { get; set; }
        }

        public class DetalleFactura
        {
            public string codigoPrincipal { get; set; }
            public string codigoAuxiliar { get; set; }
            public string descripcion { get; set; }
            public int cantidad { get; set; }
            public double precioUnitario { get; set; }
            public decimal descuento { get; set; }
            public double precioTotalSinImpuesto { get; set; }
            public List<DetalleImpuesto> detalleImpuesto { get; set; }
            public List<DetalleAdicional> detalleAdicional { get; set; }
        }

        public class FormaPago
        {
            public string formaPago { get; set; }
            public int plazo { get; set; }
            public string unidadTiempo { get; set; }
            public double total { get; set; }
        }

        public class TotalImpuesto
        {
            public int codigo { get; set; }
            public object codigoPorcentaje { get; set; }
            public int tarifa { get; set; }
            public int descuentoAdicional { get; set; }
            public double baseImponible { get; set; }
            public double valor { get; set; }
        }

        public class InfoAdicional
        {
            public string nombre { get; set; }
            public string valor { get; set; }
        }

        public class Request
        {
            public int compania { get; set; }
            public int tipoEmision { get; set; }
            public string tipoDocumento { get; set; }
            public string establecimiento { get; set; }
            public string puntoEmision { get; set; }
            public string secuencial { get; set; }
            public string claveAcceso { get; set; }
            public string fechaEmision { get; set; }
            public string tipoIdentificacionComprador { get; set; }
            public string guiaRemision { get; set; }
            public string razonSocialComprador { get; set; }
            public string identificacionComprador { get; set; }
            public double totalSinImpuestos { get; set; }
            public double totalDescuento { get; set; }
            public int propina { get; set; }
            public double importeTotal { get; set; }
            public string moneda { get; set; }
            public string email { get; set; }
            public int contigenciaDet { get; set; }
            public string estado { get; set; }
            public string ambiente { get; set; }
            public string ruc { get; set; }
            public string codigoNumerico { get; set; }
            public List<DetalleFactura> detalleFactura { get; set; }
            public List<FormaPago> formaPago { get; set; }
            public List<TotalImpuesto> totalImpuesto { get; set; }
            public List<InfoAdicional> infoAdicional { get; set; }
            public string datosVendedor { get; set; }
            public string observacionesFactura { get; set; }
            public string ciudad { get; set; }
            public string direccion { get; set; }
            public string fechaVencimiento { get; set; }

            public string codSucursal { get; set; }
        }

        public class Root
        {
            public List<Request> _request { get; set; }
        }



    }

}
