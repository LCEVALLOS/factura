﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoViamaticaPica.Modal
{
    public class GuiaRemisionJson
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        public class InfoAdicional
        {
            public string nombre { get; set; }
            public string valor { get; set; }
        }

        public class Adicional
        {
            public string identificacionDestinatario { get; set; }
            public string codigoInterno { get; set; }
            public string nombre { get; set; }
            public string valor { get; set; }
        }

        public class Detalle
        {
            public string identificacionDestinatario { get; set; }
            public string codigoInterno { get; set; }
            public string codigoAdicional { get; set; }
            public string descripcion { get; set; }
            public string cantidad { get; set; }
            public List<Adicional> adicional { get; set; }
        }

        public class Destinatario
        {
            public string identificacionDestinatario { get; set; }
            public string razonSocialDestinatario { get; set; }
            public string direccionDestinatario { get; set; }
            public string motivoTraslado { get; set; }
            public string documentoAduaneroUnico { get; set; }
            public string codigoEstablecimientoDestino { get; set; }
            public string ruta { get; set; }
            public string tipoDocumentoSustento { get; set; }
            public string numeroDocumentoSustento { get; set; }
            public string numeroAutorizacionDocumentoSustento { get; set; }
            public string fechaEmisionDocumentoSustento { get; set; }
            public List<Detalle> detalle { get; set; }
        }

        public class Request
        {
            public string idGuiaRemision { get; set; }
            public int compania { get; set; }
            public string establecimiento { get; set; }
            public string puntoEmision { get; set; }
            public string secuencial { get; set; }
            public object claveAcceso { get; set; }
            public int tipoEmision { get; set; }
            public string direccionPartida { get; set; }
            public string razonSocialTransportista { get; set; }
            public string tipoIdentificacionTransportista { get; set; }
            public string rucTransportista { get; set; }
            public string rise { get; set; }
            public string fechaIniTransporte { get; set; }
            public string fechaFinTransporte { get; set; }
            public string placa { get; set; }
            public string email { get; set; }
            public string ambiente { get; set; }
            public string codigoNumerico { get; set; }
            public string ruc { get; set; }
            public string contigenciaDet { get; set; }
            public List<InfoAdicional> infoAdicional { get; set; }
            public List<Destinatario> destinatario { get; set; }
            public string codSucursal { get; set; }
        }

        public class Root
        {
            public List<Request> _request { get; set; }
        }


    }
}
