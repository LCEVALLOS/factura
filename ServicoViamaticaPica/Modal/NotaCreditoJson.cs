﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoViamaticaPica.Modal
{
    public class NotaCreditoJson
    {
        public class DetalleImpuesto
        {
            public string codigoInterno { get; set; }
            public int codigo { get; set; }
            public string codigoPorcentaje { get; set; }
            public int tarifa { get; set; }
            public double baseImponible { get; set; }
            public double valor { get; set; }

        }
        public class DetalleAdicional
        {
            public string codigoPrincipal { get; set; }
            public string nombre { get; set; }
            public string valor { get; set; }

        }
        public class Detalle
        {
            public string codigoInterno { get; set; }
            public string codigoAdicional { get; set; }
            public string descripcion { get; set; }
            public int cantidad { get; set; }
            public double precioUnitario { get; set; }
            public int descuento { get; set; }
            public double precioTotalSinImpuesto { get; set; }
            public IList<DetalleImpuesto> detalleImpuesto { get; set; }
            public IList<DetalleAdicional> detalleAdicional { get; set; }

        }
        public class InfoAdicional
        {
            public string nombre { get; set; }
            public string valor { get; set; }

        }
        public class TotalImpuesto
        {
            public int codigo { get; set; }
            public string codigoPorcentaje { get; set; }
            public double baseImponible { get; set; }
            public double valor { get; set; }

        }
        public class _request
        {
            public int compania { get; set; }
            public int tipoEmision { get; set; }
            public string claveAcceso { get; set; }
            public string tipoDocumento { get; set; }
            public string establecimiento { get; set; }
            public string puntoEmision { get; set; }
            public string secuencial { get; set; }
            public string fechaEmision { get; set; }
            public string tipoIdentificacionComprador { get; set; }
            public string razonSocialComprador { get; set; }
            public string identificacionComprador { get; set; }
            public string tablaMurano { get; set; }
            public string rise { get; set; }
            public string tipoDocumentoModificado { get; set; }
            public string numeroDocumentoModificado { get; set; }
            public string fechaEmisionDocumentoModificado { get; set; }
            public double totalSinImpuestos { get; set; }
            public double valorModificacion { get; set; }
            public string moneda { get; set; }
            public string motivo { get; set; }
            public int contigenciadet { get; set; }
            public string email { get; set; }
            public string ambiente { get; set; }
            public string codigoNumerico { get; set; }
            public string ruc { get; set; }
            public IList<Detalle> detalle { get; set; }
            public IList<InfoAdicional> infoAdicional { get; set; }
            public IList<TotalImpuesto> totalImpuesto { get; set; }
            public string codSucursal { get; set; }

        }
        public class Application
        {
            public IList<_request> _request { get; set; }

        }
    }
}
