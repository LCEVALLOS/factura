﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoViamaticaPica.Modal
{
    public class RespuestaJson
    {
        public string clave_acceso { get; set; }
        public string estado { get; set; }

        public RespuestaJson(string claveAcceso, string estado)
        {
            this.clave_acceso = claveAcceso;
            this.estado = estado;

        }
        public RespuestaJson(string claveAcceso)
        {
            this.clave_acceso = claveAcceso;
        }
    }
}
