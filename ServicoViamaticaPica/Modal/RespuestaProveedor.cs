﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoViamaticaPica.Modal
{
    public class RespuestaProveedor
    {
       
            public string numeroAutorizacion { get; set; }
            public int codigo { get; set; }
            public string estado { get; set; }
            public object xmlAutorizado { get; set; }
            public string errorSRI { get; set; }
            public object xmlRespuesaAutorizacion { get; set; }
            public string mensaje { get; set; }
            public object fechaAutorizacion { get; set; }
        
    }
}
