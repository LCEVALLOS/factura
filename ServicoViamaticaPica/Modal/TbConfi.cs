﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoViamaticaPica.Modal
{
    public class Data
    {
        public string cod_tabla { get; set; }
        public string cod_tipo { get; set; }
        public object des_tipo { get; set; }
        public object tip_modulo { get; set; }
        public string descripcion { get; set; }
        public string seleccionado { get; set; }
        public string estado { get; set; }
        public string codigo_adicional1 { get; set; }
        public string codigo_adicional2 { get; set; }
        public object fec_ult_sync { get; set; }
        public object id_proceso_sync { get; set; }
        public object estado_sync { get; set; }
    }

    public class Registros
    {
        public List<Data> data { get; set; }
    }

    public class resurl
    {
        public string ok { get; set; }
        public string msj { get; set; }
        public Registros registros { get; set; }
        public string publicKey { get; set; }
    }
}
