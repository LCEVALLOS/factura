﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaGeneraXml.ModalEndpoint
{
    public class ModalCatalogoCliente
    {

        public string canal { get; set; }
        public string usuario { get; set; }

        public ModalCatalogoCliente(string _canal, string _usuario)
        {
            canal = _canal;
            usuario = _usuario;
        }

    }
}
