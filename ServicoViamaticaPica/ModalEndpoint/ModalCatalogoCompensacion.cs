﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaGeneraXml.ModalEndpoint
{
    public class ModalCatalogoCompensacion
    {
        public string cod_empresa { get; set; }
        public string fecha_desde { get; set; }
        public string fecha_hasta { get; set; }
    }
}
