﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaGeneraXml.ModalEndpoint
{
    public class ModalCatalogoProductos
    {
        public string cod_empresa { get; set; }
        public string canal { get; set; }
        public string tipo_material { get; set; }
        public string bodega { get; set; }


        public ModalCatalogoProductos(string _cod_empresa, string  _canal, string _tipo_material, string _bodega)
        {
            cod_empresa = _cod_empresa;
            canal = _canal;
            tipo_material = _tipo_material;
            bodega = _bodega;
        }
    }

}
