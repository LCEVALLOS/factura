﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaGeneraXml.ModalEndpoint
{
    public class ModalDepositosCajerosReportados
    {
        public string cod_organizacion { get; set; }
        public string fecha_desde { get; set; }
        public string fecha_hasta { get; set; }
        public string tipo { get; set; }
    }
}
