﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaGeneraXml.ModalEndpoint
{
    public class ModalDescuento
    {
        public string canal { get; set; }
        public string grupo_cliente { get; set; }
        public string material { get; set; }
        public ModalDescuento(string _canal,string _grupo_cliente,string _material)
        {
            canal = _canal;
            grupo_cliente = _grupo_cliente;
            material = _material;
        }
    }
}
