﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaGeneraXml.ModalEndpoint
{
    public class ModalDescuentoProductos
    {
        public string canal { get; set; }
        public string grupo_cliente { get; set; }
        public string material { get; set; }
    }
}
