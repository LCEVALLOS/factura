﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoViamaticaPica.ModalEndpoint
{
    public class ModalListarIngresarStockProductos
    {
        public string bodega { get; set; }

        public ModalListarIngresarStockProductos(string _bodega)
        {
            bodega = _bodega;
        }
    }
}
