﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaGeneraXml.ModalEndpoint
{
    public class ModalMotivosPedidos
    {
        public string canal { get; set; }
        public string tipo_pedido { get; set; }
    }
}
