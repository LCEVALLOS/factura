﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoViamaticaPica.ModalEndpoint
{
    class RespuestaPost
    {
        public string codigo { get; set; }
        public string mensaje { get; set; }
    }
}
