﻿
namespace ServicoViamaticaPica
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.ServicioMuranoFeSap = new System.ServiceProcess.ServiceProcessInstaller();
            this.MuranoFeSap = new System.ServiceProcess.ServiceInstaller();
            // 
            // ServicioMuranoFeSap
            // 
            this.ServicioMuranoFeSap.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.ServicioMuranoFeSap.Password = null;
            this.ServicioMuranoFeSap.Username = null;
            // 
            // MuranoFeSap
            // 
            this.MuranoFeSap.Description = "Servicio VIAMATICA S.A";
            this.MuranoFeSap.ServiceName = "MuranoFeSap";
            this.MuranoFeSap.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            this.MuranoFeSap.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.serviceInstaller1_AfterInstall);
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.ServicioMuranoFeSap,
            this.MuranoFeSap});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller ServicioMuranoFeSap;
        private System.ServiceProcess.ServiceInstaller MuranoFeSap;
    }
}