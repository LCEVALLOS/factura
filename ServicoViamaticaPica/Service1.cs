﻿using System.Configuration;
using System.ServiceProcess;
using System.Timers;
using System;

namespace ServicoViamaticaPica
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }

        Timer timer;

        protected override void OnStart(string[] args)
        {
            try
            {
                int ProcesoNormal = int.Parse(ConfigurationManager.AppSettings["ProcesoNormal"]);
                timer = new Timer(ProcesoNormal);
                timer.Elapsed += new System.Timers.ElapsedEventHandler(this.Timer_Elapsed_Ciclo);
                timer.Start();
            }
            catch (Exception ex)
            {
                try
                {
                    OnStop();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        private void Timer_Elapsed_Ciclo(object sender, System.Timers.ElapsedEventArgs e)
        {
            TimerProces TM = new TimerProces();
            try
            {
                TM.StarServ();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                timer.Start();
            }
        }

        protected override void OnStop()
        {
        }
    }
}
