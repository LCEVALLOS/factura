﻿using ServicoViamaticaPica.LogicaFacturaElec;
using ServicoViamaticaPica.LogicaMiddleware;
using ServicoViamaticaPica.Modal;
using System;
using System.Collections.Generic;

namespace ServicoViamaticaPica
{
    public class StartProces
    {
        private readonly ProcesFacElec _procesFacElec = new ProcesFacElec();
        private readonly ProcesEnvioSoap _procesEnvioSoap = new ProcesEnvioSoap();

        public void IniProce(List<Data> data)
        {
            try
            {
             _procesFacElec.FacturacionElectronica(data);
              // _procesEnvioSoap.EnvioSoap();



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
