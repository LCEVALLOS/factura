﻿using ServicoViamaticaPica.Conexion;
using ServicoViamaticaPica.Modal;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace ServicoViamaticaPica
{
    public class TimerProces
    {
        private readonly StartProces _startProces = new StartProces();
        private readonly ContexMurano _urlMurano = new ContexMurano();

        public void StarServ()
        {
            try
            {
                List<Data> consulta = _urlMurano.ConsultaMurano();
                var intHoraSystema = Convert.ToInt32(DateTime.Now.ToString("H:mm").Trim().Replace(":", ""));

                var horaIni = consulta.Where(x => x.descripcion.Contains("rango_inicio_servicio")).Select(x => x.codigo_adicional1).FirstOrDefault();
                var horaFin = consulta.Where(x => x.descripcion.Contains("rango_final_servicio")).Select(x => x.codigo_adicional1).FirstOrDefault();
                
                var intHoraInicio = Convert.ToInt32(horaIni.Trim().Replace(":", ""));
                var intHoraFin = Convert.ToInt32(horaFin.Trim().Replace(":", ""));
                var ProcesoNormal = int.Parse(ConfigurationManager.AppSettings["ProcesoNormal"]);

                if (intHoraSystema >= intHoraInicio && intHoraSystema <= intHoraFin)
                {
                    _startProces.IniProce(consulta);
                }
            }
      
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
